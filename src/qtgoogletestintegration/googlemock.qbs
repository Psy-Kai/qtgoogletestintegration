import qbs
import qbs.FileInfo
import "googlecommon.js" as googleCommon

StaticLibrary {
    name: "GoogleMock"

    readonly property string googletestDir: FileInfo.joinPaths(sourceDirectory, "..", "..",
                                                               "third_party", "googletest")

    cpp.cxxLanguageVersion: "c++11"
    cpp.defines: [ "GTEST_LANG_CXX11" ]
    cpp.dynamicLibraries: {
        if (qbs.hostOS.contains("windows")) {
            return [];
        } else {
            return [ "pthread" ];
        }
    }
    cpp.warningLevel: "none"

    cpp.includePaths: [].concat(googleCommon.getGTestIncludes(qbs, googletestDir))
    .concat(googleCommon.getGMockIncludes(qbs, googletestDir))

    files: [
        "eventlistener_p.h",
        "eventlistener.cpp",
        "testcasebase.cpp",
        "testcasebase.h",
    ].concat(googleCommon.getGTestAll(qbs, googletestDir))
    .concat(googleCommon.getGMockAll(qbs, googletestDir))

    Depends {
        name: "Qt"
        submodules: [
            "test"
        ]
    }
    Export {
        cpp.defines: product.cpp.defines
        cpp.includePaths: [
            FileInfo.joinPaths(product.sourceDirectory, "..")
        ].concat(product.cpp.includePaths)
        cpp.dynamicLibraries: product.cpp.dynamicLibraries
        Depends {
            name: "cpp"
        }
    }
}
