#pragma once
#ifndef QTGOOGLETESTINTEGRATION_TESTCASEBASE_H
#define QTGOOGLETESTINTEGRATION_TESTCASEBASE_H

namespace qtgoogletestintegration {
class TestCaseBase
{
public:
    TestCaseBase();
    virtual ~TestCaseBase() = 0;
};
} // namespace qtgoogletestintegration

#endif // QTGOOGLETESTINTEGRATION_TESTCASEBASE_H
