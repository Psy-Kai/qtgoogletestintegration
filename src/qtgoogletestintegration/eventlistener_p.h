#pragma once
#ifndef QTGOOGLETESTINTEGRATION_EVENTLISTENER_P_H
#define QTGOOGLETESTINTEGRATION_EVENTLISTENER_P_H

#include <gtest/gtest.h>

namespace qtgoogletestintegration {
class EventListener final : public ::testing::EmptyTestEventListener
{
public:
    ~EventListener() override;
    void OnTestPartResult(const testing::TestPartResult &test_part_result) override;
};
} // namespace qtgoogletestintegration

#endif // QTGOOGLETESTINTEGRATION_EVENTLISTENER_P_H
