#include "eventlistener_p.h"

#include <QDebug>
#include <QTest>

namespace qtgoogletestintegration {

EventListener::~EventListener() = default;


void EventListener::OnTestPartResult(const testing::TestPartResult &test_part_result)
{
    if (test_part_result.file_name())
        qInfo().noquote() << test_part_result.file_name() << ':' << test_part_result.line_number();
    QFAIL(test_part_result.summary());
}

} // namespace qtgoogletestintegration
