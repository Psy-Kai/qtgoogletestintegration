#include "testcasebase.h"

#include <gmock/gmock.h>
#include "eventlistener_p.h"

namespace qtgoogletestintegration {

TestCaseBase::TestCaseBase()
{
    int argc = 0;
    ::testing::InitGoogleMock(&argc, static_cast<char**>(nullptr));
    auto &listeners = ::testing::UnitTest::GetInstance()->listeners();
    delete listeners.Release(listeners.default_result_printer());
    listeners.Append(new EventListener);
}

TestCaseBase::~TestCaseBase() = default;

} // namespace qtgoogletestintegration
