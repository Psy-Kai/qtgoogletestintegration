import qbs

Project {
    name: "Qt Googletest Integration"

    references: [
        "src/qtgoogletestintegration",
    ]
}
