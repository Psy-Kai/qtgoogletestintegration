# QtGoogleTestIntegration

This repository contains some helper classes to create an autotest with [QtTest](http://doc.qt.io/qt-5/qtest-overview.html) using [GoogleMock](https://github.com/google/googletest/blob/master/googlemock/docs/ForDummies.md).